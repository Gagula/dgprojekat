<?php
    namespace App\Models; 

    use App\Core\DatabaseConnection;
    use App\Core\Model;
    use App\Core\Field;
    use App\Validators\NumberValidator;
    use App\Validators\StringValidator;
    use \PDO;

    class ClientModel extends Model {
        
        protected function getFields(): array {
            return [
                'client_id'   => new Field((new NumberValidator())->setInteger(10), false),
                
                'first_name'  => new Field((new StringValidator())->setMaxLength(255)),
                'last_name'   => new Field((new StringValidator())->setMaxLength(255)),
                'email'       => new Field((new StringValidator())->setMaxLength(255)),
                'phone'       => new Field((new StringValidator())->setMaxLength(45))
            ];
        }

        public function getByLastname(string $lastname){
            return $this->getTableName('last_name',$lastname);
            /*$sql  = 'SELECT * FROM client WHERE last_name= ?;';
            $prep = $this->getConnection()->prepare($sql);
            $res  = $prep->execute([$lastname]);
            $client = NULL;
            if ($res){
                $client = $prep->fetch(\PDO::FETCH_OBJ);
            }
                return $client;*/
        }
        public function getByFirstname(string $firstname){
            return $this->getTableName('first_name', $firstname);
            /*$sql  = 'SELECT * FROM client WHERE first_name= ?;';
            $prep = $this->getConnection()->prepare($sql);
            $res  = $prep->execute([$firstname]);
            $client = NULL;
            if ($res){
                $client = $prep->fetch(\PDO::FETCH_OBJ);
            }
                return $client;*/
        }
    }
