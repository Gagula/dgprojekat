<?php
    namespace App\Models;

    use App\Core\DatabaseConnection;
    use App\Core\Model;
    use App\Core\Field;
    use App\Validators\NumberValidator;
    use App\Validators\StringValidator;
    use \PDO;

    class RoomModel extends Model {

        protected function getFields(): array {
            return [
                'room_id'           => new Field((new NumberValidator())->setInteger(10), false),
                
                'room_name'         => new Field((new StringValidator())->setMaxLength(45)),
                'bed_capacity'      => new Field((new NumberValidator())->setInteger(1)),
                'floor_id'          => new Field((new NumberValidator())->setInteger(10))
            ];
        }
        
        public function getByRoomId(int $roomId){
            $sql  = 'SELECT * FROM room WHERE room_id= ?;';
            $prep = $this->getConnection()->prepare($sql);
            $res  = $prep->execute([$roomId]);
            $room = NULL;
            if ($res){
                $room = $prep->fetch(\PDO::FETCH_OBJ);
            }
                return $room;
        }

        public function getByFloorId(int $floorId){
            $sql  = 'SELECT * FROM room WHERE floor_id= ?;';
            $prep = $this->getConnection()->prepare($sql);
            $res  = $prep->execute([$floorId]);
            $room = NULL;
            if ($res){
                $room = $prep->fetch(\PDO::FETCH_OBJ);
            }
                return $room;
        }

        public function getByRoomName(string $roomName): string {
            $sql  = 'SELECT * FROM room WHERE room_name= ?;';
            $prep = $this->getConnection()->prepare($sql);
            $res  = $prep->execute([$roomName]);
            $room = NULL;
            if ($res){
                $room = $prep->fetch(\PDO::FETCH_OBJ);
            }
                return $room;
        }
        
        public function getByBedCapacity(int $bedCapacity): array {
            $sql  = 'SELECT * FROM room WHERE bed_capacity= ?;';
            $prep = $this->getConnection()->prepare($sql);
            $res  = $prep->execute([$bedCapacity]);
            $rooms = [];
            if ($res){
                $rooms = $prep->fetchAll(\PDO::FETCH_OBJ);
            }
                return $rooms;
        }
    }
