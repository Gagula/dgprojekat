<?php
    namespace App\Core;

    use App\Core\DatabaseConnection;
    use App\Core\Model;
    use App\Core\Field;
    
    class ReservationViewModel extends Model {

        protected function getAll() {
            $sql  = "SELECT
            'room_reservation' AS type,
            room_reservation.room_reservation_id AS id,
            room_reservation.start_at AS starts_at,
            room_reservation.ends_at,
            room_reservation.user_id,
            room_reservation.client_id,
            room_reservation.room_id AS room_id,
            NULL AS hall_id 
            FROM
            room_reservation
            UNION
            SELECT
            'hall_reservation' AS type,
            hall_reservation.hall_reservation_id AS id,
            hall_reservation.starts_at AS starts_at,
            hall_reservation.ends_at,
            hall_reservation.user_id,
            hall_reservation.client_id,
            NULL AS room_id,
            hall_reservation.hall_id AS hall_id
            FROM
            hall_reservation;";
            $prep = $this->getConnection()->prepare($sql);
            $res  = $prep->execute();
            $reservations = [];
            if ($res){
                $reservations = $prep->fetchAll(\PDO::FETCH_OBJ);
            }
                return $reservations;
        }
    }