<?php
    namespace App\Models;

    use App\Core\DatabaseConnection;
    use App\Core\Model;
    use App\Core\Field;
    use App\Validators\NumberValidator;
    use App\Validators\StringValidator;
    use App\Validators\DateTimeValidator;
    use \PDO;

    class HallReservationModel extends Model{

        protected function getFields(): array {
            return [
                'hall_reservation_id'   => new Field((new NumberValidator())->setInteger(10), false),
                
                'starts_at'             => new Field((new DateTimeValidator())->allowDate()->allowTime(), false),
                'ends_at'               => new Field((new DateTimeValidator())->allowDate()->allowTime()),
                'user_id'               => new Field((new NumberValidator())->setInteger(10)),
                'client_id'             => new Field((new NumberValidator())->setInteger(10)),
                'hall_id'               => new Field((new NumberValidator())->setInteger(10))
            ];
        }

       
        public function getByHallReservedtId(int $hallReservedId){
            $sql  = 'SELECT * FROM hall_reservation WHERE hall_reservation_id= ?;';
            $prep = $this->getConnection()->prepare($sql);
            $res  = $prep->execute([$hallReservedId]);
            $hallReserved = NULL;
            if ($res){
                $hallReserved = $prep->fetch(\PDO::FETCH_OBJ);
            }
                return $hallReserved;
        }

        public function getByClientId(int $clientId){
            $sql  = 'SELECT * FROM hall_reservation WHERE client_id= ?;';
            $prep = $this->getConnection()->prepare($sql);
            $res  = $prep->execute([$clientId]);
            $clientReserved = NULL;
            if ($res){
                $clientReserved = $prep->fetch(\PDO::FETCH_OBJ);
            }
                return $clientReserved;
        }

        
        public function getByHallId(int $hallId){
            $sql  = 'SELECT * FROM hall_reservation WHERE hall_id= ?;';
            $prep = $this->getConnection()->prepare($sql);
            $res  = $prep->execute([$hallId]);
            $hallReserved = NULL;
            if ($res){
                $hallReserved = $prep->fetch(\PDO::FETCH_OBJ);
            }
                return $hallReserved;
        }

        public function getByUserId(int $userId){
            $sql  = 'SELECT * FROM hall_reservation WHERE `user_id`= ?;';
            $prep = $this->getConnection()->prepare($sql);
            $res  = $prep->execute([$userId]);
            $userReserved = NULL;
            if ($res){
                $userReserved = $prep->fetch(\PDO::FETCH_OBJ);
            }
                return $userReserved;
        }
        
    }