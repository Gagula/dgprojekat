<?php
    namespace App\Models;

    use App\Core\DatabaseConnection;
    use App\Core\Model;
    use App\Core\Field;
    use App\Validators\NumberValidator;
    use App\Validators\StringValidator;
    use \PDO;
     
    class HallModel extends Model{
        
        protected function getFields(): array {
            return [
                'hall_id'               => new Field((new NumberValidator())->setInteger(10), false),
                
                'hall_name'             => new Field((new StringValidator())->setMaxLength(255)),
                'seating_capacity'      => new Field((new NumberValidator())->setInteger(3)),
                'floor_id'              => new Field((new NumberValidator())->setInteger(10))
            ];
        }

        public function getByHallId(int $hallId){
            $sql  = 'SELECT * FROM hall WHERE hall_id= ?;';
            $prep = $this->getConnection()->prepare($sql);
            $res  = $prep->execute([$hallId]);
            $hall = NULL;
            if ($res){
                $hall = $prep->fetch(\PDO::FETCH_OBJ);
            }
                return $hall;
        }
        public function getByFloorId(int $floorId){
            $sql  = 'SELECT * FROM hall WHERE floor_id= ?;';
            $prep = $this->getConnection()->prepare($sql);
            $res  = $prep->execute([$floorId]);
            $hall = NULL;
            if ($res){
                $hall = $prep->fetch(\PDO::FETCH_OBJ);
            }
                return $hall;
        }

        
        public function getByHallName(string $hallName): string {
            $sql  = 'SELECT * FROM hall WHERE hall_name= ?;';
            $prep = $this->getConnection()->prepare($sql);
            $res  = $prep->execute([$hallName]);
            $hall = NULL;
            if ($res){
                $hall = $prep->fetch(\PDO::FETCH_OBJ);
            }
                return $hall;
        }

        public function getBySeatingCapacity(int $seatingCapacity): array {
            $sql  = 'SELECT * FROM hall WHERE seating_capacity= ?;';
            $prep = $this->getConnection()->prepare($sql);
            $res  = $prep->execute([$seatingCapacity]);
            $halls = [];
            if ($res){
                $halls = $prep->fetchAll(\PDO::FETCH_OBJ);
            }
                return $halls;
        }
    }
