<?php 
    namespace App\Models;
    
    use App\Core\DatabaseConnection;
    use App\Core\Model;
    use App\Core\Field;
    use App\Validators\NumberValidator;
    use App\Validators\StringValidator;
    use \PDO;

    class UserModel extends Model {
        
        protected function getFields(): array {
            return [
                'user_id'       => new Field((new NumberValidator())->setInteger(10), false),
                
                'first_name'    => new Field((new StringValidator())->setMaxLength(255)),
                'last_name'     => new Field((new StringValidator())->setMaxLength(255)),
                'email'         => new Field((new StringValidator())->setMaxLength(255)),
                'username'      => new Field((new StringValidator())->setMaxLength(255)),
                'password_hash' => new Field((new StringValidator())->setMaxLength(128))
            ];
        }
        /*public function getById(int $userId){
            $sql  = 'SELECT * FROM user WHERE user_id= ?;';
            $prep = $this->dbc->getConnection()->prepare($sql);
            $res  = $prep->execute([$userId]);
            $user = NULL;
            if ($res){
                $user = $prep->fetch(\PDO::FETCH_OBJ);
            }
                return $user;
        }

        public function getALL(): array {
            $sql  = 'SELECT * FROM user ;';
            $prep = $this->dbc->getConnection()->prepare($sql);
            $res  = $prep->execute();
            $users = [];
            if ($res){
                $users = $prep->fetchALL(\PDO::FETCH_OBJ);
            }
                return $users;
        }*/
        public function getByLastname(string $lastname){
            return $this->getTableName('last_name',$lastname);
            /*$sql  = 'SELECT * FROM user WHERE last_name= ?;';
            $prep = $this->dbc->getConnection()->prepare($sql);
            $res  = $prep->execute([$lastname]);
            $user = NULL;
            if ($res){
                $user = $prep->fetch(\PDO::FETCH_OBJ);
            }
                return $user;*/
        }
        public function getByFirstname(string $firstname){
            return $this->getTableName('first_name', $firstname);
            /*$sql  = 'SELECT * FROM user WHERE first_name= ?;';
            $prep = $this->dbc->getConnection()->prepare($sql);
            $res  = $prep->execute([$firstname]);
            $user = NULL;
            if ($res){
                $user = $prep->fetch(\PDO::FETCH_OBJ);
            }
                return $user;*/
        }
        public function postLogin() {
            $username  = filter_input(INPUT_POST, 'login_username', FILTER_SANITIZE_STRING);
            $password  = filter_input(INPUT_POST, 'login_password', FILTER_SANITIZE_STRING);

            $validator = (new StringValidator())->setMinLength(6)->setMaxLength(120);
            if (!$validator->isValid($password)) {
                $this->set('message', 'Lozinka mora imati najmanje 6 karaktera i najvise 120 karaktera.');
                return;
            }

            $userModel = new UserModel($this->getDatabaseConnection());
            
            $user = $userModel->getByFieldName('username', $username);
            
            if (!$user) {
                $this->set('message', 'Doslo je do greske: Ne postoji korisnik sa tim korisnickim imenom');
                return;
            }

            $passwordHash = $user->password_hash;
            
            if (!password_verify($password, $passwordHash)) {
                sleep(1); #srecavanje brute-force napada na lozinke na jedan pokusaj u sec
                $this->set('message', 'Doslo je do greske: Nije dobra lozinka');
                return;
            }
            $this->getSession()->put('user_id', $user->user_id);
            

            ob_clean();
            \header('Location: ' . \Configuration::BASE . 'user/profile');
            exit;

        }
    }