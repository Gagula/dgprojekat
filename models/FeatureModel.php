<?php
    namespace App\Models;

    use App\Core\DatabaseConnection;
    use App\Core\Model;
    use App\Core\Field;
    use App\Validators\NumberValidator;
    use App\Validators\StringValidator;
    use \PDO;

    class FeatureModel extends Model {
        
        protected function getFields(): array {
            return [
                'feature_id'    => new Field((new NumberValidator())->setInteger(10), false),
                
                'feature_name'  => new Field((new StringValidator())->setMaxLength(255))
            ];
        }

        /*public function getByFeatureId(int $featureId){
            return $this->getById('feature_id',$featureId);
            /*$sql  = 'SELECT * FROM feature WHERE feature_id= ?;';
            $prep = $this->dbc->getConnection()->prepare($sql);
            $res  = $prep->execute([$featureId]);
            $feature = NULL;
            if ($res){
                $feature = $prep->fetch(\PDO::FETCH_OBJ);
            }
                return $feature;
        }*/

        
        public function getByFeatureName(string $featurename){
            return $this->getTableName('feature_name', $featurename );
            /*$sql  = 'SELECT * FROM feature WHERE feature_name= ?;';
            $prep = $this->dbc->getConnection()->prepare($sql);
            $res  = $prep->execute([$featurename]);
            $feature = NULL;
            if ($res){
                $feature = $prep->fetch(\PDO::FETCH_OBJ);
            }
                return $feature;*/
        }

        #Joinovi 
		public function getAllLinkedToRoomById(int $roomId) {
			$sql = 'SELECT feature.* FROM feature INNER JOIN room_feature ON feature.feature_id = room_feature.feature_id WHERE room_feature.room_id = ?;';
			$prep = $this->getConnection()->prepare($sql);
            $res  = $prep->execute([$roomId]);
            $features = [];
            if ($res){
                $features = $prep->fetchAll(\PDO::FETCH_OBJ);
            }
			return $features;
		}

		public function getAllLinkedToHallById(int $hallId) {
			$sql = 'SELECT feature.* FROM feature INNER JOIN hall_feature ON feature.feature_id = hall_feature.feature_id WHERE hall_feature.hall_id = ?;';
			$prep = $this->getConnection()->prepare($sql);
            $res  = $prep->execute([$hallId]);
            $features = [];
            if ($res){
                $features = $prep->fetchAll(\PDO::FETCH_OBJ);
            }
			return $features;
		}
    }
