<?php
    namespace App\Models;

    use App\Core\DatabaseConnection;
    use App\Core\Model;
    use App\Core\Field;
    use App\Validators\NumberValidator;
    use App\Validators\StringValidator;
    use \PDO;


    class FloorModel extends Model{

        protected function getFields(): array {
            return [
                'floor_id'    => new Field((new NumberValidator())->setInteger(10), false),
                
                'floor_name'  => new Field((new StringValidator())->setMaxLength(65))
            ];
        }

        
        /*public function getByFloorId(int $floorId){
            $sql  = 'SELECT * FROM `floor` WHERE floor_id= ?;';
            $prep = $this->dbc->getConnection()->prepare($sql);
            $res  = $prep->execute([$floorId]);
            $floor = NULL;
            if ($res){
                $floor = $prep->fetch(\PDO::FETCH_OBJ);
            }
                return $floor;
        }

        public function getAll(): array {
            $sql  = 'SELECT * FROM `floor` ;';
            $prep = $this->dbc->getConnection()->prepare($sql);
            $res  = $prep->execute();
            $floors = [];
            if ($res){
                $floors = $prep->fetchALL(\PDO::FETCH_OBJ);
            }
                return $floors;
        }
        public function getByFloorName(string $floorname){
            $sql  = 'SELECT * FROM `floor` WHERE floor_name= ?;';
            $prep = $this->dbc->getConnection()->prepare($sql);
            $res  = $prep->execute([$floorname]);
            $floor = NULL;
            if ($res){
                $floor = $prep->fetch(\PDO::FETCH_OBJ);
            }
                return $floor;
        }*/
      
    }