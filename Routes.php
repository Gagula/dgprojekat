<?php
    use App\Core\Route;
    return [
        Route::get('|^user/register/?$|',                  'Main' ,         'getRegister'),
        Route::post('|^user/register/?$|',                 'Main' ,         'postRegister'),
        Route::get('|^user/login/?$|',                     'Main',          'getLogin'),
        Route::post('|^user/login/?$|',                    'Main',          'postLogin'),
        Route::post('|^user/logout/?$|',                   'Main',          'postLogout'),
       


        Route::get('|^client/([0-9]+)/?$|',                'Client',        'show'),
        Route::get('|^client/([0-9]+)/?$|',                'Client',        'delete'),
        Route::get('|^user/([0-9]+)/?$|',                  'User',          'show'),

        Route::get('|^login/?$|',                          'User',          'postLogin'),    

        Route::get('|^room/([0-9]+)/?$|',                  'Room',          'show'),
        Route::get('|^hall/([0-9]+)/?$|',                  'Hall',          'show'),
        Route::get('|^feature/([0-9]+)/?$|',               'Feature',       'show'),
        Route::get('|^floor/([0-9]+)/?$|',                 'Floor',         'show'),

        
        #API rute
        Route::get('|^api/client/([0-9]+)/?$|',            'ApiClient',     'show'),
        Route::get('|^api/user/([0-9]+)/?$|',              'ApiUser',       'show'),
        Route::get('|^api/bookmarks/?$|',                  'ApiBookmark',   'getBookmarks'),
        Route::get('|^api/bookmarks/add/([0-9]+)/?$|',     'ApiBookmark',   'getBookmarks'),
        Route::get('|^api/bookmarks/clear/?$|',            'ApiBookmark',   'getBookmarks'),

        #User role routes:
        Route::get('|^user/profile/?$|',                   'UserDashboard', 'index'),
        Route::get('|^user/clients/?$|',                   'UserClientManagement', 'clients'),
        Route::get('|^user/clients/edit/([0-9]+)/?$|',     'UserClientManagement', 'getEdit'),
        Route::post('|^user/clients/edit/([0-9]+)/?$|',    'UserClientManagement', 'postEdit'),
        Route::get('|^user/clients/add/?$|',               'UserClientManagement', 'getAdd'),
        Route::post('|^user/clients/add/?$|',              'UserClientManagement', 'postAdd'),

        Route::get('|^user/rooms/?$|',                     'UserRoomManagement', 'rooms'),
        Route::get('|^user/rooms/edit/([0-9]+)/?$|',       'UserRoomManagement', 'getEdit'),
        Route::post('|^user/rooms/edit/([0-9]+)/?$|',      'UserRoomManagement', 'postEdit'),
        Route::get('|^user/rooms/add/?$|',                 'UserRoomManagement', 'getAdd'),
        Route::post('|^user/rooms/add/?$|',                'UserRoomManagement', 'postAdd'),

        Route::any('|^.*$|',                               'Main', 'home')
    ];