<?php
    namespace App\Controllers;

    class UserController extends \App\Core\Controller {
        private $dbc;

        
        public function show($id){
            $userModel = new \App\Models\UserModel($this->getDatabaseConnection());
            $user = $userModel->getById($id);
            $this->set('user', $user);
            
            $roomModel = new \App\Models\RoomModel($this->getDatabaseConnection());
            $rooms = $roomModel->getByBedCapacity($id);
            $this->set('rooms', $rooms);
           
            $hallModel = new \App\Models\HallModel($this->getDatabaseConnection());
            $hall = $hallModel ->getByHallId($id);
            $this->set('hall', $hall);
        }

    }