<?php
    namespace App\Controllers;

    use App\Core\ApiController;
    use App\Models\ClientModel;

    class ApiClientController extends ApiController {

        public function show($id){
            $clientModel = new ClientModel($this->getDatabaseConnection());
            $client = $clientModel->getById($id);
            $this->set('client', $client);

        }
    }