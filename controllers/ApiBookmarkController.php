<?php
    namespace App\Controllers;

    use App\Core\ApiController;
    use App\Models\ClientModel;

    class ApiBookmarkController extends ApiController {
        public function getBookmarks() {
            $bookmarks = $this->getSession()->get('bookmarks', []);
            $this->set('bookmarks', $bookmarks);
            $this->set('error', 0);
        }

        public function addBookmark($clientId) {
            $bookmarks = $this->getSession()->get('bookmarks', []);

            $cm = new ClientModel($this->getDatabaseConnection());
            $client = $cm->getById($clientId);

            if (!$client) {
                $this->set('error', -1);
                return;
            }

            $bookmarks[] = $client;

            $this->getSession()->put('bookmarks', $bookmarks);
            $this->set('error', 0);
        }

        public function clear() {
            $this->getSession()->put('bookmarks', []);
            $this->set('error', 0);
        }
    }
