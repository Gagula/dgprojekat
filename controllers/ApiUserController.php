<?php
    namespace App\Controllers;

    use App\Core\ApiController;
    use App\Models\UserModel;

    class ApiUserController extends ApiController {

        public function show($id){
            $userModel = new UserModel($this->getDatabaseConnection());
            $user = $userModel->getById($id);
            $this->set('user', $user);

        }
    }