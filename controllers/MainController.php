<?php
    namespace App\Controllers;

    use App\Core\DatabaseConnection;
    use App\Core\Model;
    use App\Core\Field;
    use \App\Models\ClientModel;
    use \App\Models\UserModel;
    use App\Validators\NumberValidator;
    use App\Validators\StringValidator;
    use \PDO;

    class MainController extends \App\Core\Controller {
        private $dbc;

        
        public function home(){
            $clientModel = new ClientModel($this->getDatabaseConnection());
            $userModel = new UserModel($this->getDatabaseConnection());
            $clients = $clientModel->getAll();
            $users = $userModel->getAll();

            $this->set('clients', $clients);
            $this->set('users', $users);

            /*$floorModel = new \App\Models\FloorModel($this->getDatabaseConnection());
            $floorModel->add([
                'floor_name' => '9. Sprat'
            ]);*/
            
            //$this->getSession()->put('neki_kljuc', 'Neka vrednost' . \rand(100, 999));
              
        }
        public function getRegister() {

        }

        public function postRegister() {
            $username   = \filter_input(INPUT_POST, 'reg_username', FILTER_SANITIZE_STRING);
            $email      = \filter_input(INPUT_POST, 'reg_email', FILTER_SANITIZE_EMAIL);
            $firstname  = \filter_input(INPUT_POST, 'reg_first_name', FILTER_SANITIZE_STRING);
            $lastname   = \filter_input(INPUT_POST, 'reg_last_name', FILTER_SANITIZE_STRING);
            $password1  = \filter_input(INPUT_POST, 'reg_password_1', FILTER_SANITIZE_STRING);
            $password2  = \filter_input(INPUT_POST, 'reg_password_2', FILTER_SANITIZE_STRING);

           /*print_r([
                $username,
                $passwordHash,
                $email,
                $lastname,
                $firstname 
            ]);
            exit;*/
            if ($password1 != $password2) {
                $this->set('message', 'Morate dva puta da potvrdite istu lozinku.');
                return;
            }

            $validator = (new StringValidator())->setMinLength(6)->setMaxLength(120);
            if (!$validator->isValid($password1)) {
                $this->set('message', 'Lozinka mora imati najmanje 6 karaktera i najvise 120 karaktera.');
                return;
            }
            #provera da li postoji vec korisnik sa tim username-om vec u bazi
            $userModel = new UserModel($this->getDatabaseConnection());
            
            $user = $userModel->getByFieldName('email', $email);
            if ($user) {
                $this->set('message', 'Doslo je do greske: Vec postoji korisnik sa tim e-mailom');
            }

            $user = $userModel->getByFieldName('username', $username);
            if ($user) {
                $this->set('message', 'Doslo je do greske: Vec postoji korisnik sa tim username-om');
            }
            #hashiranje pasworda radi dopremanja hasha u bazu 
            $passwordHash = \password_hash($password1, PASSWORD_DEFAULT);

            $userId = $userModel->add([
                'username'          => $username,
                'password_hash'     => $passwordHash,
                'email'             => $email,
                'last_name'         => $lastname,  #mora pocinjati VELIKIM slovom zbog trigera u bazi 
                'first_name'        => $firstname  #mora pocinjati VELIKIM slovom zbog trigera u bazi
            ]);
            

            if (!$userId) {
                $this->set('message', 'Doslo je do greske: Nalog nije uspesno registrovan');
            }
            $this->set('message', 'Nalog je uspesno registrovan.');
        }

        public function getLogin() {

        }

        public function postLogin() {
            $username  = filter_input(INPUT_POST, 'login_username', FILTER_SANITIZE_STRING);
            $password  = filter_input(INPUT_POST, 'login_password', FILTER_SANITIZE_STRING);

            $validator = (new StringValidator())->setMinLength(6)->setMaxLength(120);
            if (!$validator->isValid($password)) {
                $this->set('message', 'Lozinka mora imati najmanje 6 karaktera i najvise 120 karaktera.');
                return;
            }

            $userModel = new UserModel($this->getDatabaseConnection());
            
            $user = $userModel->getByFieldName('username', $username);
            
            if (!$user) {
                $this->set('message', 'Doslo je do greske: Ne postoji korisnik sa tim korisnickim imenom');
                return;
            }

            $passwordHash = $user->password_hash;
            
            if (!password_verify($password, $passwordHash)) {
                sleep(1); #srecavanje brute-force napada na lozinke na jedan pokusaj u sec
                $this->set('message', 'Doslo je do greske: Nije dobra lozinka');
                return;
            }
            $this->getSession()->put('user_id', $user->user_id);
            

            ob_clean();
            \header('Location: ' . \Configuration::BASE . 'user/profile');
            exit;

        }

        public function getLogout() {
            $this->getSession()->remove('user_id');
            
            ob_clean();
            \header('Location: ' . \Configuration::BASE);
            exit;

        }


    }