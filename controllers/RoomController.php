<?php
    namespace App\Controllers;

    class RoomController extends \App\Core\Controller {
        private $dbc;

        public function show($id){ 
            $roomModel = new \App\Models\RoomModel($this->getDatabaseConnection());
            $room = $roomModel->getByRoomId($id);
            $this->set('room', $room);
			
			$featureModel = new \App\Models\FeatureModel($this->getDatabaseConnection());
			$roomFeatures = $featureModel->getAllLinkedToRoomById(intval($room->room_id));
            $this->set('features', $roomFeatures);
        }
    }
