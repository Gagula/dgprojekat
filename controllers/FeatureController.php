<?php
    namespace App\Controllers;

    class FeatureController extends \App\Core\Controller {
        private $dbc;

        
        public function show($id){ 
            $featureModel = new \App\Models\FeatureModel($this->getDatabaseConnection());
            $feature = $featureModel->getByFeatureId($id);
            $this->set('feature', $feature);
           
        }

    }