<?php
    namespace App\Controllers;

    use App\Core\Controller;
    use App\Models\ClientModel;

    class UserClientManagementController extends \App\Core\Role\UserRoleController{
        public function clients(){
            $clientModel = new ClientModel($this->getDatabaseConnection());
            $clients = $clientModel->getAll();
            $this->set('clients', $clients);
            
        }

        public function getEdit($clientId) {
            $clientModel = new ClientModel($this->getDatabaseConnection());
            $client = $clientModel->getById($clientId);

            if (!$client){
                $this->redirect(\Configuration::BASE . 'user/clients');
            }
            if($client->user_id != $this->getSession()->get('user_id')){
                $this->redirect( \Configuration::BASE . 'user/clients');
                return; 
            }
            $this->set('client', $client);
            return $clientModel;

            $roomModel= new \App\Models\RoomModel($this->getDatabaseConnection());
            $rooms=$roomModel->getAll();
            $this->set('room',$rooms);
        }

        public function postEdit($clientId) {
            $clientModel = new ClientModel($this->getDatabaseConnection());
            $clientModel = $this->getEdit($clientId);

            $editData=[
                'first_name' => \filter_input(INPUT_POST, 'first_name' , FILTER_SANITIZE_STRING),
                'last_name'  => \filter_input(INPUT_POST, 'last_name' , FILTER_SANITIZE_STRING),
                'email'      => \filter_input(INPUT_POST, 'email' , FILTER_SANITIZE_STRING),
                'phone'     => \filter_input(INPUT_POST, 'phone' , FILTER_SANITIZE_STRING),
                'user_id' =>$this->getSession()->get('user_id')
            ];

            $res= $clientModel->editById($clientId,$editData);
            if(!$res){
                $this->set('message','Nije bilo moguće izmeniti clienta.');
                return;
            }

            if(isset($_FILES['image']) && ($_FILES)['image']['error'] == 0){
                $uploadStatus=$this->doImageUpload('image',$clientId);
                if(!$uploadStatus){
                    return; 
                }
            }

            $this->redirect(\Configuration::BASE . 'user/clients');
        }

        public function getAdd(){

        }

        public function postAdd(){
            $firstname = \filter_input(INPUT_POST, 'first_name' , FILTER_SANITIZE_STRING);
            $lastname = \filter_input(INPUT_POST, 'last_name' , FILTER_SANITIZE_STRING);

            $clientModel = new ClientModel($this->getDatabaseConnection());
            $clientId = $clientModel->add([
                'first_name' => \filter_input(INPUT_POST, 'first_name' , FILTER_SANITIZE_STRING),
                'last_name'  => \filter_input(INPUT_POST, 'last_name' , FILTER_SANITIZE_STRING),
                'email'      => \filter_input(INPUT_POST, 'email' , FILTER_SANITIZE_STRING),
                'phone'     => \filter_input(INPUT_POST, 'phone' , FILTER_SANITIZE_STRING),
                'user_id' =>$this->getSession()->get('user_id')
            ]);

            if ($clientId) {
                $this->redirect(\Configuration::BASE . 'user/clients');
            }

            $this->set('message', 'Doslo je do greske: Nije moguce dodati ovog klienta');
            
        }
        private function doImageUpload(string $fieldName, string $fileName):bool{

            unlink(\Configuration::UPLOAD_DIR . $fileName . '.jpg');
            
            $uploadPath= new \Upload\Storage\FileSystem(\Configuration::UPLOAD_DIR);
            $file= new \Upload\File($fieldName,$uploadPath);
            $file->setName($fileName);
            $file->addValidations([
                new \Upload\Validation\Mimetype("image/jpeg"),
                new \Upload\Validation\Size("5M")
            ]);

            try{
                $file->upload();
                return true;
            }catch(Exception $e){
                $this->set('message','Greška: ' . implode(', ', $file->getErrors()));
                return false;
            }
        }
    }