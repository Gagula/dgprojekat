<?php
    namespace App\Controllers;

    class HallController extends \App\Core\Controller {
        private $dbc;

        
        public function show($id){ 
            $hallModel = new \App\Models\HallModel($this->getDatabaseConnection());
            $hall = $hallModel->getByHallId($id);
            $this->set('hall', $hall);
           
        }

    }