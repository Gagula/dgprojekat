<?php
    namespace App\Controllers;

    class ClientController extends \App\Core\Controller {
        private $dbc;

        
        public function show($id){
            $clientModel = new \App\Models\ClientModel($this->getDatabaseConnection());
            $client = $clientModel->getById($id);
            $this->set('client', $client);
            
            $roomModel = new \App\Models\RoomModel($this->getDatabaseConnection());
            $rooms = $roomModel->getByBedCapacity($id);
            $this->set('rooms', $rooms);
           
            $hallModel = new \App\Models\HallModel($this->getDatabaseConnection());
            $hall = $hallModel ->getByHallId($id);
            $this->set('hall', $hall);
        }

    }