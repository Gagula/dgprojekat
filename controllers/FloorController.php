<?php
    namespace App\Controllers;

    class FloorController extends \App\Core\Controller {
        private $dbc;

        
        public function show($id){ 
            $floorModel = new \App\Models\FloorModel($this->getDatabaseConnection());
            $floor = $floorModel->getByFloorId($id);
            $this->set('floor', $floor);
           
        }

    }