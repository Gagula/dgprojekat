<?php
    namespace App\Controllers;

    use App\Core\Controller;
    use App\Models\RoomModel;
    use App\Models\UserModel;

    class UserRoomManagementController extends \App\Core\Role\UserRoleController{
        public function rooms(){
            $roomModel = new RoomModel($this->getDatabaseConnection());
            $rooms = $roomModel->getAll();
            $this->set('rooms', $rooms);
            
        }
        public function getEdit($roomId) {
            $roomModel = new RoomModel($this->getDatabaseConnection());
            $room = $roomModel->getById($roomId);

            if (!$room){
                $this->redirect(\Configuration::BASE . 'user/rooms');
            }
            if($room->user_id != $this->getSession()->get('user_id')){
                $this->redirect( \Configuration::BASE . 'user/rooms');
                return; 
            }
            $this->set('room', $room);
            return $roomModel;

            $rooms=$roomModel->getAll();
            $this->set('room',$rooms);
        }

        public function postEdit($roomId) {
            $roomModel = $this->getEdit($roomId);

            $editData=[
                'room_name' => \filter_input(INPUT_POST, 'room_name' , FILTER_SANITIZE_STRING),
                'bed_capacity'  => \filter_input(INPUT_POST, 'bed_capacity' , FILTER_SANITIZE_STRING),
                'floor_id'     => \filter_input(INPUT_POST, 'floor_id' , FILTER_SANITIZE_STRING),
                'user_id' =>$this->getSession()->get('user_id')
            ];

            $roomModel = new RoomModel($this->getDatabaseConnection());

            $res= $roomModel->editById($roomId,$editData);
            if(!$res){
                $this->set('message','Nije bilo moguće izmeniti clienta.');
                return;
            }

            if(isset($_FILES['image']) && ($_FILES)['image']['error'] == 0){
                $uploadStatus=$this->doImageUpload('image',$roomId);
                if(!$uploadStatus){
                    return; 
                }
            }

            $this->redirect(\Configuration::BASE . 'user/rooms');
        }

        public function getAdd(){

        }

        public function postAdd($roomId){
            $roomModel = new RoomModel($this->getDatabaseConnection());
           
            $roomId = $roomModel->add([
                'room_name'     => \filter_input(INPUT_POST, 'room_name' , FILTER_SANITIZE_STRING),
                'bed_capacity'  => \filter_input(INPUT_POST, 'bed_capacity' , FILTER_SANITIZE_STRING),
                'floor_id'      => \filter_input(INPUT_POST, 'floor_id' , FILTER_SANITIZE_STRING),
                'user_id'       =>$this->getSession()->get('user_id')
            ]);

            if ($roomId) {
                $this->redirect(\Configuration::BASE . 'user/rooms');
            }

            if(isset($_FILES['image']) && ($_FILES)['image']['error'] == 0){
                $uploadStatus=$this->doImageUpload('image',$roomId);
                if(!$uploadStatus){
                    return; 
                }
            }

            $this->set('message', 'Doslo je do greske: Nije moguce dodati ovu sobu');
               

        }
        private function doImageUpload(string $fieldName, string $fileName):bool{

            unlink(\Configuration::UPLOAD_DIR . $fileName . '.jpg');
            
            $uploadPath= new \Upload\Storage\FileSystem(\Configuration::UPLOAD_DIR);
            $file= new \Upload\File($fieldName,$uploadPath);
            $file->setName($fileName);
            $file->addValidations([
                new \Upload\Validation\Mimetype("image/jpeg"),
                new \Upload\Validation\Size("5M")
            ]);

            try{
                $file->upload();
                return true;
            }catch(Exception $e){
                $this->set('message','Greška: ' . implode(', ', $file->getErrors()));
                return false;
            }
        }

    }
    