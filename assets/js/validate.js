
function validateFormRegister() {
    let status = true;

    document.querySelector('#error-message').classList.add('d-none');
    document.querySelector('#error-message').innerHTML = '';
    
    const email = document.querySelector('#input_email').value;
    if (!email.match(/^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)(\.[a-zA-Z]{2,5}){1,3}$/)) {
        document.querySelector('#error-message').innerHTML += 'Email mora biti u formatu example@mail.domen...</br>';
        document.querySelector('#error-message').classList.remove('d-none');
        status = false;
    }

    const firstname = document.querySelector('#input_first_name').value;
    if (!firstname.match(/^[A-ZŠĐŽČĆ][a-zšdžčć]+([ \\-][A-ZŠĐŽČĆ][a-zšdžčć]+)?$/)) {
        document.querySelector('#error-message').innerHTML += 'Ime mora pocinjati velikim slovom i mora imati najmanje 2 uzastopna vidljiva karaktera...</br>';
        document.querySelector('#error-message').classList.remove('d-none');
        status = false;
    }

    const lastname = document.querySelector('#input_last_name').value;
    if (!lastname.match(/^[A-ZŠĐŽČĆ][a-zšdžčć]+([ \\-][A-ZŠĐŽČĆ][a-zšdžčć]+)?$/)) {
        document.querySelector('#error-message').innerHTML += 'Prezime mora pocinjati velikim slovom i mora imati najmanje 2 uzastopna vidljiva karaktera...</br>';
        document.querySelector('#error-message').classList.remove('d-none');
        status = false;
    }

    const username = document.querySelector('#input_username').value;
    if (!username.match(/.*[^\s]{2,}.*/)) {
        document.querySelector('#error-message').innerHTML += 'Username mora imati najmanje 2 uzastopna vidljiva karaktera...</br>';
        document.querySelector('#error-message').classList.remove('d-none');
        status = false;
    }

    return status;
}