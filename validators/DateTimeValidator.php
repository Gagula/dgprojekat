<?php 
    namespace App\Validators;

    use App\Core\Validator;

    class DateTimeValidator implements Validator {
        private $isDateAllowed;
        private $isTimeAllowed;

        public function __construct() {
            $this->isDateAllowed = true;
            $this->isTimeAllowed = true;
        }

        public function &allowDate(): DateTimeValidator {
            $this->isDateAllowed = true;
            return $this;
        }

        public function &allowTime(): DateTimeValidator {
            $this->isTimeAllowed = true;
            return $this;
        }

        public function &disallowDate(): DateTimeValidator {
            $this->allowedDate = false;

            if ($this->allowedTime == false) {
                $this->allowedTime = true;
            }

            return $this;
        }

        public function &disallowTime(): DateTimeValidator {
            $this->allowedTime = false;

            if ($this->allowedDate == false) {
                $this->allowedDate = true;
            }

            return $this;
        }

        public function isValid(string $value): bool {
            $components = [];

            if ($this->allowedDate) {
                $components[] = '[0-9]{4}-[0-9]{2}-[0-9]{2}';
            }

            if ($this->allowedTime) {
                $components[] = '[0-9]{2}:[0-9]{2}:[0-9]{2}';
            }

            $pattern = \implode(' ', $components);

            return \preg_match('/^' . $pattern . '$/', $value);
        }
    }