<?php
    namespace App\Validators;

    use App\Core\Validator;

    class BitValidator implements Validator {
        public function isValid(string $value): bool {
            return in_array($value, [0, 1]);
        }
    }