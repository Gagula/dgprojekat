<?php
    require_once 'Configuration.php';
    require_once 'vendor/autoload.php';

    ob_start();

    use App\Core\DatabaseConfiguration;
    use App\Core\DatabaseConnection;
    use App\Models\UserModel;
    use App\Models\ClientModel;
    use App\Core\Router;
    use App\Core\Session\Session;

    $databaseConfiguration = new DatabaseConfiguration (
        Configuration::DATABASE_HOST,
        Configuration::DATABASE_USER,
        Configuration::DATABASE_PASS,
        Configuration::DATABASE_NAME
    );
    $databaseConnection = new DatabaseConnection($databaseConfiguration);

    $url = strval(filter_input(INPUT_GET, 'URL'));
    $httpMethod = filter_input(INPUT_SERVER, 'REQUEST_METHOD');


    $router = new Router();
    $routes =  require_once 'Routes.php';
    foreach ($routes as $route){
        $router->add($route);
    }

    $route = $router->find($httpMethod, $url);
    $arguments = $route->extractArguments($url);
    

    $fullControllerName = '\\App\\Controllers\\'. $route->getControllerName() . 'Controller';
    $controller = new $fullControllerName($databaseConnection);

    
    $sessionStorageClassName = Configuration::SESSION_STORAGE;
    $sessionStorage = new $sessionStorageClassName(...Configuration::SESSION_STORAGE_DATA);
    $session = new Session($sessionStorage);

    $fingerprintProviderFactoryClass  = Configuration::FINGERPRINT_PROVIDER_FACTORY;
    $fingerprintProviderFactoryMethod = Configuration::FINGERPRINT_PROVIDER_METHOD;
    $fingerprintProviderFactoryArgs = Configuration::FINGERPRINT_PROVIDER_ARGS;
    $fingerprintProviderFactory = new $fingerprintProviderFactoryClass;
    $fingerprintProvider = $fingerprintProviderFactory->$fingerprintProviderFactoryMethod(...$fingerprintProviderFactoryArgs);
    
    $session->setFingerprintProvider($fingerprintProvider);
   
    $controller->setSession($session);
    $controller->getSession()->reload();
    $controller->__pre(); 
    call_user_func_array([$controller, $route->getMethodName()], $arguments);
    $controller->getSession()->save();

    $data = $controller->getData();


    if ($controller instanceof \App\Core\ApiController) {
        ob_clean();
        header('Content-type: aplication/json; charset=utf-8');
        header('Access-Control-Allow-Origin: *'); #nije pametno dozvoliti ovo za sve ali radi testiranja je trenutno dozvoljeno:D 
        echo json_encode($data);
        exit;

    }

    $loader = new \Twig\Loader\FilesystemLoader("./views");
    $twig = new \Twig\Environment($loader,[
        #"cache"=> "./twig-cache",
        "auto_reload" => true
    ]);

    $data['BASE'] = Configuration::BASE;

    echo $twig->render($route->getControllerName() . '/' . $route->getMethodName() . '.html', $data );
